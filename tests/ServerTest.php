<?php

namespace Sabentis\SmtpServer\Tests;

use Psr\Log\NullLogger;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Symfony\Component\EventDispatcher\DependencyInjection\RegisterListenersPass;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Sabentis\SmtpServer\ServerSymfony;
use Sabentis\SmtpServer\Message;
use Sabentis\SmtpServer\Session;
use Sabentis\SmtpServer\SessionInterface;
use Sabentis\SmtpServer\MessageReceivedEvent;
use Sabentis\HomersTulebox\UtilsPhp;

use React\Promise\Deferred;
use React\EventLoop\Factory as EventLoopFactory;

use PHPUnit\Framework\TestCase;

class ServerTest extends TestCase {

  /**
   * Assert a specific SMTP code in a response message.
   *
   * @param string $message
   * @param string $code
   */
  protected function assertSmtpCode(string $message, string $code) {
    $cmd = trim(substr($message, 0, 4));
    $this->assertEquals($cmd, $code);
  }

  /**
   * Client connection.
   *
   * @var \React\Socket\Connection
   */
  protected $connection;

  /**
   * Message body
   *
   * @var string
   */
  protected $sample_message_body;

  /**
   * Expiration timer.
   *
   * @var \React\EventLoop\Timer\TimerInterface
   */
  protected $timer;

  /**
   * Test basic server usage.
   */
  public function testSetupServerAndReceiveMessage() {

    // Number of seconds for the test to timeout.
    $test_timeout = 5;

    Core::initialize();

    $loop = Core::looper();

    $errNo = 0;
    $errStr = '';
    $stream = @stream_socket_client('tcp://127.0.0.1:53521', $errNo, $errStr, 10);
    $this->connection = new \React\Socket\Connection($stream, $loop);

    $deferred = new \React\Promise\Deferred();

    $this->connection->once('data',
        function ($data, $conn) use ($deferred) {
          $deferred->resolve(array($data, $conn));
        }
    );

    $this->sample_message_body = <<<EOF
Dear whoever is in charge,

This is a smple e-mail.

Best regards,

David

EOF;

    $instance = $this;

    // This test should not take longer than 5 seconds.
    $this->timer = $loop->addTimer($test_timeout, function() use ($instance) {
      $instance->fail("Test timed out. Loop stopped.");
      Core::looper()->stop();
    });

    // Rejection callback.
    $onReject = function() {
      Core::looper()->stop();
    };

    $promise = $deferred->promise()
        ->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $instance->connection->write("HELO mytestserver.com\r\n");
          $instance->connection->once('data',
              function ($data) use ($deferred) {
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )
        ->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $credentials = base64_encode("brokenauth");
          $instance->connection->write("AUTH PLAIN {$credentials}\r\n");
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '535');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )
        ->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $credentials = base64_encode("\0badusername\0badpassword");
          $instance->connection->write("AUTH PLAIN {$credentials}\r\n");
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '535');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )
        ->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $credentials = base64_encode("\0username\0password");
          $instance->connection->write("AUTH PLAIN {$credentials}\r\n");
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '235');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )
        ->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $instance->connection->write("RSET");
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '250');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )
        ->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $instance->connection->write("AUTH LOGIN");
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '334');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )
        ->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $instance->connection->write(base64_encode("username"));
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '334');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )
        ->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $instance->connection->write(base64_encode("password"));
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '235');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )
        ->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $instance->connection->write("RSET");
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '250');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )
        ->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $instance->connection->write("AUTH LOGIN " . base64_encode('username'));
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '334');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )
        ->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $instance->connection->write(base64_encode("password"));
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '235');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )
        ->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $instance->connection->write("MAIL FROM: <testing@phpsmtp.com>\r\n");
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '250');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )
        ->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $instance->connection->write("RCPT TO: <this-email-does-not-exist-asdf1234@gmail.com>\r\n");
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '250');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $instance->connection->write("RCPT TO: <this-email-does-not-exist-asdf123@gmail.com>\r\n");
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '250');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $instance->connection->write("DATA\r\n");
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '354');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $instance->connection->write($instance->sample_message_body . Session::DATA_EOF);
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '250');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )->then(function ($data) use ($instance) {
          $deferred = new Deferred();
          $instance->connection->write("QUIT\r\n");
          $instance->connection->once('data',
              function ($data) use ($deferred, $instance) {
                $instance->assertSmtpCode($data, '221');
                $deferred->resolve($data);
              }
          );
          return $deferred->promise();
        }
        )->then(function ($data) use ($instance) {
          // This is where it should all finish.
          $instance->connection->close();
          $instance->timer->cancel();
          Core::looper()->stop();
        })
    ;

    // Make sure that when the message is received the information is OK.
    Core::getServer()->on(SessionInterface::EVENT_SMTP_RECEIVED, function(Message $message) use ($instance) {
      $instance->assertEquals($message->data, $instance->sample_message_body);
    });

    $promise->then(null, $onReject);

    Core::looper()->run();
  }

}

/**
 * Our implementation for AUTH and DELIVERY of this SMTP session.
 */
class CustomSmtpSession extends Session {

  /**
   * {@inheritdoc}
   */
  protected function dataFinishedHandler() : string {
    $this->log->info("Message delivered succesfully.");
    return '';
  }

  /**
   * {@inheritdoc}
   */
  protected function authenticate(string $auth_id, string $user_id, string $password) : bool {
    return $user_id == 'username' && $password == "password";
  }

}

class MessageHandler implements EventSubscriberInterface {

  /**
   * Summary of $messageQueue
   *
   * @var MessageReceivedEvent[]
   */
  protected $messageQueue = [];

  public static function getSubscribedEvents() {
    return [SessionInterface::EVENT_SMTP_RECEIVED => ['onMessageReceived', 0]];
  }

  public function onMessageReceived(MessageReceivedEvent $event) {
    Core::logger()->info("Message received event called.");
  }
}

class Core {

  /**
   * Service container
   *
   * @var \Symfony\Component\DependencyInjection\ContainerBuilder
   */
  public static $container;

  /**
   * Get the looper service.
   *
   * @return \React\EventLoop\LoopInterface
   */
  public static function looper() {
    return self::$container->get('looper');
  }

  /**
   * Get the logger service.
   *
   * @return \Psr\Log\LoggerInterface
   */
  public static function logger() {
    return self::$container->get('logger');
  }

  /**
   * Get the SMTP server instance.
   *
   * @return \Sabentis\SmtpServer\ServerSymfony
   */
  public static function getServer() {
    return self::$container->get('smtpserver');
  }

  /**
   * Get the message handler service.
   *
   * @return MessageHandler
   *
   */
  public static function message_handler() {
    return self::$container->get('messagehandler');
  }


  /**
   * Initialize the service container.
   */
  public static function initialize() {

    $container = &self::$container;

    $container = new ContainerBuilder(new ParameterBag());
    $container->addCompilerPass(new RegisterListenersPass('event_dispatcher', 'event_listener', 'event_subscriber'));

    // Register the event dispatcher service
    $container->setDefinition('event_dispatcher', new Definition(
        ContainerAwareEventDispatcher::class,
        [new Reference('service_container')]
    ));

    // Register the loop service
    $container->register('looper', \React\EventLoop\LoopInterface::class)
      ->setFactory(UtilsPhp::callbackStatic(EventLoopFactory::class, 'create'));

    $container->register('logger', \SimpleLogger\Stdout::class);

    // Register the smtp server service
    $container->register('smtpserver', ServerSymfony::class)
      ->addArgument('127.0.0.1')
      ->addArgument(53521)
      ->addArgument(new Reference('logger'))
      ->addArgument(new Reference('looper'))
      ->addArgument(CustomSmtpSession::class)
      ->addArgument(new Reference('event_dispatcher'));


    // Register the tests service
    $container->register('messagehandler', MessageHandler::class)
      ->addTag('event_subscriber');

    // Compile the container
    $container->compile();

    /** @var \Sabentis\SmtpServer\ServerSymfony */
    $server_service = $container->get('smtpserver');
    $server_service->run();
  }

}