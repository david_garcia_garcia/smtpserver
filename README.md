# SMTP Server Implementation using ReactPHP

Provides an SMTP server implementation using ReactPHP. The PHP script needs to run on an elevated prompt
in order to bind to the specified port.

The main usage of this library is to provide SMTP interfaces to custom HTTP based mail sending libraries.

To use, you need to implement a SMTP handler class that takes care of authentication and delivery.

```php

<?php

include_once 'vendor/autoload.php';

use Sabentis\SmtpServer\Server;
use Sabentis\SmtpServer\Session;

use React\EventLoop\Factory as EventLoopFactory;

/**
 * Our implementation for AUTH and DELIVERY of this SMTP server.
 */
class CustomSmtpSession extends Session {

  /**
   * {@inheritdoc}
   */
  protected function authenticate(string $auth_id, string $user_id, string $password) : bool {
    // You can also "not" check auth here, and fail on
    // dataFinishedHandler() during delivery if credentials
    // are wrong. Credentials are stored in the message itself.
    return $user_id == 'username' && $password == "password";
  }

  /**
   * {@inheritdoc}
   */
  protected function dataFinishedHandler() : string {
    $this->log->info(json_encode($this->message));
    return '';
  }

}

/**
 * Sample SMTP server
 */
class SampleServer {

  public function start() {
    $loop = EventLoopFactory::create();
    $logger = new \SimpleLogger\Stdout();
    $server = new Server('127.0.0.1', 53521, $logger, $loop, CustomSmtpSession::class);
    $server->run();
    $loop->run();
  }

}

$server = new SampleServer();
$server->start();

```